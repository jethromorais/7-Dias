# 7Dias.

Software para identificação visual dos agentes da prefeitura de belo horizonte usando o Google Maps, desenvolvido no Hackathon da UniParty.

#Estado Atual do Projeto
Já finalizado o processo de construção básica do software, atendendo os requisitos mínimos exigidos pela Avsystem e PBH.

#REQUISITOS:
xAMPP
php 5.6
jQUERY 3.3.1
Materializecss
PHP VaderLib
CSS 3

#Processo de instalação/configuração
Instalação padrão de todas as aplicações mencionadas anteriormente
Clonar o repositório e salvar dentro da estrutura de páginas do Apache
Executar o Sevendays.sql para criação dos requisitos do MySQL
Acesso a aplicação pela URL: http://localhost/app.php
Para acesso de outras estações substituir localhost pelo IP do servidor

#Explicando a aplicação
Aplicação pode ser iniciada através http://localhost/app.php, acessando a tela de credenciamento, utilizando usuário em branco e senha em branco. 
Assim que iniciado a sessão, é registrado no banco de dados as informações do agente, como ID, Localização, garantindo a rastreabilidade de todos os agentes

#Localizando os agentes
Basta acessar a URL: http://localhost/mapa.php

Job Done !

Como funciona programaticamente:
Antes de qualquer coisa, é preciso alterar as conexões do banco de dados no arquivo
Sevendays\htdocs\applications\core\configDB.php
Neste arquivo, você deve inserir as informações do banco de dados atual.
Após essa configuração, salve o arquivo e mande executar o Projeto.
Observações sobre o banco de dados. 
A ideia no banco de dados é inserir as informações (na tabela agente) com o status 0 (ID escolhido, de status, para informar que o agente está ativo e habilitado para aparecer no mapa)
E a função colocar você deve escrever (tudo em upercase) alguma desses itens:
BHTRANS, SLU, RESOLVE, GMBH, SAMU.
Caso precise fazer alguma alteração, a nivel programatico. A estrutura foi criada utilizando conceitos dos padrões de projetos, builder, observer, reflection, factory, namespace etc. 
O conceito é MVC e por isso existe a pasta controllers, onde são identificadas com nomes de classes Controller -> Exemplo: localizacaoController. E nela conterão as action (ações que serão chamadas ao acessar x URL.)
Para o acesso ao banco de dados temos as classes de modelos, localizadas na pasta models.
Sinta-se livre para alterar qualquer informação.


